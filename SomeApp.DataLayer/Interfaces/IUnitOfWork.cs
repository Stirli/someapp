﻿using System;
using SomeApp.DataLayer.Entities;

namespace SomeApp.DataLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Group> Groups { get; }
        IRepository<Student> Students { get; }
        void Save();
    }

}