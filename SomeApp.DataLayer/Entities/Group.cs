﻿using System;
using System.Collections.Generic;

namespace SomeApp.DataLayer.Entities
{
    public class Group
    {
        public Group()
        {
            Students = new List<Student>();
        }
        public int GroupId { get; set; }
        public string CourseName { get; set; }
        public DateTime Commence { get; set; }
        public decimal BasePrice { get; set; }
        // навигационное свойство
        public List<Student> Students { get; set; }
    }

}