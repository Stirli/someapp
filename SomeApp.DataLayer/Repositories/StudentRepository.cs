﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SomeApp.DataLayer.EFContext;
using SomeApp.DataLayer.Entities;
using SomeApp.DataLayer.Interfaces;

namespace SomeApp.DataLayer.Repositories
{
    class StudentRepository : IRepository<Student>
    {
        CoursesContext context;
        public StudentRepository(CoursesContext context)
        {
            this.context = context;
        }
        public void Create(Student t)
        {
            context.Students.Add(t);
        }
        public void Delete(int id)
        {
            var group = context.Students.Find(id);
            context.Students.Remove(group);
        }
        public IEnumerable<Student> Find(Func<Student, bool> predicate)
        {
            return context
                .Students
                .Include(g => g.Group)
                .Where(predicate)
                .ToList();
        }
        public Student Get(int id)
        {
            return context.Students.Find(id);
        }
        public IEnumerable<Student> GetAll()
        {
            return context.Students.Include(g => g.Group);
        }
        public void Update(Student t)
        {
            context.Entry<Student>(t).State = EntityState.Modified;
        }
    }

}