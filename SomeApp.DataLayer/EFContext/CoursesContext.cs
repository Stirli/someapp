﻿using System.Data.Entity;
using SomeApp.DataLayer.Entities;

namespace SomeApp.DataLayer.EFContext
{
    public class CoursesContext : DbContext
    {
        public CoursesContext(string name) : base(name)
        {
            Database.SetInitializer(new CourcesInitializer());
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
    }

}