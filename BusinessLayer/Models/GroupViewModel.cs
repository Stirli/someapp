﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SomeApp.BusinessLayer.Models
{
    public class GroupViewModel
    {
        public GroupViewModel()
        {
            Students = new ObservableCollection<StudentViewModel>();
        }
        public int GroupId { get; set; }
        public string CourseName { get; set; }
        public DateTime Commence { get; set; }
        public decimal BasePrice { get; set; }
        // навигационное свойство
        public ObservableCollection<StudentViewModel> Students { get; set; }
    }

}