﻿using AutoMapper;
using SomeApp.BusinessLayer.Interfaces;
using SomeApp.BusinessLayer.Models;
using SomeApp.DataLayer.Entities;
using SomeApp.DataLayer.Interfaces;
using SomeApp.DataLayer.Repositories;
using System;
using System.Collections.ObjectModel;

namespace SomeApp.BusinessLayer.Services
{
    public class GroupServise : IGroupService
    {
        static GroupServise()
        {
            // Конфигурировани AutoMapper
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<StudentViewModel, Student>();
                cfg.CreateMap<Group, GroupViewModel>();
                cfg.CreateMap<Student, StudentViewModel>();
            });
        }

        private IUnitOfWork dataBase;
        public GroupServise(string name)
        {
            dataBase = new EntityUnitOfWork(name);
        }
        public void AddStudentToGroup(int droupId, StudentViewModel
            student)
        {
            var group = dataBase.Groups.Get(droupId);
            // Конфигурировани AutoMapper

            // Отображение объекта StudentViewModel на объект Student
            var stud = Mapper.Map<Student>(student);
            // Определение цены для студента
            stud.IndividualPrice = student.HasDiscount == true
                ? group.BasePrice * (decimal)0.8
                : group.BasePrice;
            // Добавить студента
            @group.Students.Add(stud);

            // Сохранить изменения
            dataBase.Save();
        }
        public void CreateGroup(GroupViewModel group) => throw new NotImplementedException();
        public void DeleteGroup(int groupId) => throw new NotImplementedException();
        public GroupViewModel Get(int id) => throw new NotImplementedException();
        public ObservableCollection<GroupViewModel> GetAll()
        {

            // Отображение List<Group> на ObservableCollection<GroupViewModel>
            var groups = Mapper.Map<ObservableCollection<GroupViewModel>>(dataBase.Groups.GetAll());
            return groups;
        }
        public void RemoveStudentFromGroup(int droupId, int studenIdt) => throw new NotImplementedException();
        public void UpdateGroup(GroupViewModel group) => throw new NotImplementedException();
    }

}