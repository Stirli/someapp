﻿using SomeApp.BusinessLayer.Models;
using System.Collections.ObjectModel;

namespace SomeApp.BusinessLayer.Interfaces
{
    public interface IGroupService
    {
        ObservableCollection<GroupViewModel> GetAll();
        GroupViewModel Get(int id);
        void AddStudentToGroup(int droupId, StudentViewModel student);
        void RemoveStudentFromGroup(int droupId, int studenIdt);
        void CreateGroup(GroupViewModel group);
        void DeleteGroup(int groupId);
        void UpdateGroup(GroupViewModel group);
    }
}
