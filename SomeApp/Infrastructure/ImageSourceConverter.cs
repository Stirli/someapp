﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace SomeApp.Infrastructure
{
    internal class ImageSourceConverter : IValueConverter
    {
        private readonly string curDirectory = Directory.GetCurrentDirectory();

        private string ImageDirectory => Path.Combine(curDirectory, "images");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string path = value as string;
            if (string.IsNullOrEmpty(path) || !File.Exists(path))
            {
                return Path.Combine(curDirectory, "Resources", "noimage.jpg");
            }

            return Path.Combine(ImageDirectory, path);
        }
        public object ConvertBack(object value,
            Type targetType,
            object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}