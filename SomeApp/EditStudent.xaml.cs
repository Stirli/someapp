﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SomeApp.BusinessLayer.Models;

namespace SomeApp
{
    /// <summary>
    /// Логика взаимодействия для EditStudent.xaml
    /// </summary>
    public partial class EditStudent : Window
    {
        private readonly StudentViewModel _student;

        public EditStudent():this(new StudentViewModel())
        {
        }
        public EditStudent(StudentViewModel student)
        {
            InitializeComponent();
            _student = student;
            root.DataContext = _student;
        }
        private void btnOK_Click(object sender, RoutedEventArgs e) { this.DialogResult = true; }

        private void btnCancel_Click(object sender, RoutedEventArgs e) { this.Close(); }
    }
}
