﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SomeApp.BusinessLayer.Interfaces;
using SomeApp.BusinessLayer.Models;
using SomeApp.BusinessLayer.Services;

namespace SomeApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<GroupViewModel> groups;
        IGroupService groupService;
        public MainWindow()
        {
            InitializeComponent();
            groupService = new GroupServise("TestDbConnection");
            groups = groupService.GetAll();
            cBoxGroup.DataContext = groups;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var student = new StudentViewModel();
            student.DateOfBirth = new DateTime(1990, 01, 01);
            var dialog = new EditStudent(student);
            var result = dialog.ShowDialog();
            if (result == true)
            {
                var group = (GroupViewModel)cBoxGroup.SelectedItem;
                group.Students.Add(student);

                groupService.AddStudentToGroup(group.GroupId, student);
                dialog.Close();
            }
        }
    }
}
